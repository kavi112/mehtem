from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from mehtam.items import MathemItem

class Mehtem_spider(BaseSpider):
    name = "mehtam"
    allowed_domains = ["mathem.se"]
    start_urls = ["http://www.mathem.se/sok?qtype=p"]

    def __init__(self):
	    self.products=[]
	    self.n=1

    def parse(self, response):
        products1=[]
        hxs = HtmlXPathSelector(response)
        products1=hxs.select('//div[contains(@id,"ctl00_searchContent_rptProducts_")and contains(@id,"_uctlProductDisplay_divProduct")and not(contains(@id, "Count"))]/div/span/span/a/@href').extract()
        self.products+=products1
        # print(products1)
        # print(len(self.products))

        if(len(self.products)<3000):
            self.n+=1
            yield Request(url="http://www.mathem.se/sok?qtype=p&page=" + str(self.n), headers = {"Referer": "http://www.mathem.se/sok?qtype=p&page=1","X-Requested-With": "XMLHttpRequest"},callback=self.parse)
        # print("all done, count:"+str(len(self.products)))
        for i in range(len(self.products)):
        	# print self.products[i]
        	yield Request(url="http://www.mathem.se/"+self.products[i], callback=self.prod)
        	# print("\n")
        return	

    def prod(self,response):
    	items=[]
    	catPath=[]
    	hxs = HtmlXPathSelector(response)
    	for i in range(len(hxs.select('//*[@id="productNavigation"]/li'))):
            # print i
            catPath.append(hxs.select('//*[@id="productNavigation"]/li/a/span/text()').extract()[i])
            if(len(hxs.select('//*[@id="productNavigation"]/li['+str(i)+'][contains(@class,"act")]'))):
                break
        # print catPath
    	# print(response.url)
    	item = MathemItem()
    	item['productID']=''.join(hxs.select('//*[@id="ctl00_pageContent_ctrlProductDetail_hdnProductID"]/@value').extract())
    	item['productName']= ''.join(hxs.select('//*[@id="ctl00_pageContent_divMain1"]/div[2]/h1/text()').extract())
    	item['pricePerSt']=''.join(hxs.select('//*[@id="ctl00_pageContent_ctrlProductDetail_divTotalPrice"]/span/text()').extract())
    	if (len(hxs.select('//*[@id="ctl00_pageContent_ctrlProductDetail_divPrice"]/text()').extract())>0):
    	    item['pricePerKg']=hxs.select('//*[@id="ctl00_pageContent_ctrlProductDetail_divPrice"]/text()').extract()[1].strip()
    	item['productDesc']=''.join(hxs.select('//*[@id="ctl00_pageContent_divMain1"]/div[2]/div[2]/text()').extract()).strip()
    	item['categoryPath']='==>'.join(catPath)
    	item['productImage']=''.join(hxs.select('//*[@id="ctl00_pageContent_ctrlProductDetail_imgProduct"]/@src').extract())
    	items.append(item)
    	return items