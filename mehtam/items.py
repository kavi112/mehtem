# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class MathemItem(Item):
    # define the fields for your item here like:
    # name = Field()
    productID = Field()
    productName = Field()
    productImage = Field()
    pricePerKg = Field()
    pricePerSt = Field()
    productDesc = Field()
    categoryPath=Field()

    pass
