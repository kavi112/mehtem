# Scrapy settings for mehtam project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'mehtam'

SPIDER_MODULES = ['mehtam.spiders']
NEWSPIDER_MODULE = 'mehtam.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'mehtam (+http://www.yourdomain.com)'

# ITEM_PIPELINES = [
#   'scrapy_mongodb.MongoDBPipeline',
# ]
# DOWNLOAD_DELAY = 0.5
# CONCURRENT_REQUESTS = 30
# mongodb://8c7cfbf6-a08c-4913-98a2-a19498565f31:afd536b1-5454-4fde-8053-a180b25793ae@
# 10.0.55.70:25001/db
# mongodb://localhost:27017/db
# MONGODB_HOST = '10.0.55.70'
# MONGODB_POST=25001
# MONGODB_DATABASE = 'af_kavi112-kavi112'
# # mongodb://user:pass@host:port
# MONGODB_URI='mongodb://kavi112:letmein@ds043348.mongolab.com:43348/af_kavi112-kavi112'
# # 'mongodb://8c7cfbf6-a08c-4913-98a2-a19498565f31:afd536b1-5454-4fde-8053-a180b25793ae@10.0.55.70:25001/db'
# # MONGODB_URI='mongodb://localhost:27017/db'
# MONGODB_COLLECTION = 'my_cart'